Task description - task.pdf

Installation steps:
1) "composer update"
2) "php init" (0 - development environment)
3) common/config/main-local - set up database connection (You should create blank database for the project manually. I'm using mysql utf8_general_ci)
4) from root of the project run "php yii migrate/up"
5) sign up - frontend/web
6) login at backend/web - main functionality here


Database schema - schema.png
