<?php
namespace backend\models;

use common\models\Client;
use common\models\LoyaltyCard;
use yii\base\Model;

class AddClientForm extends Model
{
    public $firstName;
    public $lastName;
    public $address;
    public $email;
    public $phone;

    public $cardNumber;
    public $cardType;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'phone', 'email', 'address'], 'string'],
            [['cardNumber', 'cardType'], 'integer'],
            ['email', 'email'],
            [['firstName', 'lastName', 'phone', 'email'], 'required']
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function addClient()
    {
        $client = new Client();
        $client->first_name = $this->firstName;
        $client->last_name = $this->lastName;
        $client->address = $this->address;
        $client->email = $this->email;
        $client->phone = $this->phone;
        if (!$client->save()) {
            throw new \Exception('Error saving client');
        }

        $isCardTypeFilled = array_key_exists($this->cardType, LoyaltyCard::getCardTypes());
        if ($this->cardNumber || $isCardTypeFilled) {
            $card = new LoyaltyCard();
            $card->client_id = $client->client_id;
            $card->card_number = $this->cardNumber ? $this->cardNumber : LoyaltyCard::find()->select('max(card_number)')->scalar() + 1;
            $card->card_type = $isCardTypeFilled ? $this->cardType : LoyaltyCard::TYPE_TEMPORARY;
            if (!$card->save()) {
                throw new \Exception('Error saving card. Number should be unique.');
            }
        }
        return true;
    }
}
