<?php
namespace backend\models;

use common\models\Client;
use yii\data\ActiveDataProvider;

class ClientSearch extends Client
{
    public $card_number;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [[['card_number', 'client_id'], 'integer']]
        );
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find()->with('loyaltyCards');
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'client_id' => $this->client_id,
        ]);
        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);
        if ($this->card_number) {
            $query->joinWith('loyaltyCards');
            $query->andFilterWhere(['like', 'card_number', $this->card_number]);
        }
        return $dataProvider;
    }
}
