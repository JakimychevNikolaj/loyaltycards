<?php

namespace backend\models;

use common\models\Bill;
use common\models\Product;
use common\models\ProductList;
use yii\base\Model;

class AddBillForm extends Model
{
    public $productIds;
    public $productQuantities;
    public $loyaltyCardId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['productIds', 'productQuantities'], 'each', 'rule' => ['integer', 'min' => 1]],
            [['loyaltyCardId'], 'integer'],
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function addBill()
    {
        $productData = Product::find()
            ->select('price')
            ->where(['product_id' => $this->productIds])
            ->indexBy('product_id')
            ->column();
        $totalPrice = array_sum(array_map(
            function ($price, $quantity) {return $price * $quantity;},
            $productData,
            $this->productQuantities
        ));

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $bill = new Bill([
                'total_sum' => $totalPrice,
                'loyalty_card_id' => $this->loyaltyCardId
            ]);
            if (!$bill->save()) {
                throw new \Exception('Error saving bill');
            }

            $counter = 0;
            foreach ($productData as $productId => $price) {
                $productList = new ProductList([
                    'product_id' => $productId,
                    'bill_id' => $bill->bill_id,
                    'quantity' => $this->productQuantities[$counter],
                    'price' => $price,
                ]);
                if (!$productList->save()) {
                    throw new \Exception('Error saving product list');
                }
                $counter++;
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return true;
    }
}
