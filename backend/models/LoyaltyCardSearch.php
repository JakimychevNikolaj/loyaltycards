<?php

namespace backend\models;

use common\models\LoyaltyCard;
use yii\data\ActiveDataProvider;

class LoyaltyCardSearch extends LoyaltyCard
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoyaltyCard::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'card_type' => $this->card_type,
            'client_id' => $this->client_id
        ]);
        $query->andFilterWhere(['like', 'card_number', $this->card_number]);
        return $dataProvider;
    }
}
