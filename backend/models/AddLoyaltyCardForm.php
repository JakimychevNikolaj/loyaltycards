<?php
namespace backend\models;

use common\models\LoyaltyCard;
use yii\base\Model;
use yii\db\Exception;

class AddLoyaltyCardForm extends Model
{
    public $id;
    public $cardNumber;
    public $cardType;
    public $clientId;

    /**
     * @throws Exception
     */
    public function init()
    {
        parent::init();
        if (!$this->id) {
            return;
        }
        $card = LoyaltyCard::findOne($this->id);
        if (!$card) {
            throw new Exception('Card is not found');
        }
        $this->cardNumber = $card->card_number;
        $this->cardType = $card->card_type;
        $this->clientId = $card->client_id;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['cardNumber', 'cardType', 'clientId'], 'integer'],
            [['cardType'], 'required']
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function addCard()
    {
        if ($this->id) {
            $card = LoyaltyCard::findOne($this->id);
        } else {
            $card = new LoyaltyCard();
        }
        if (!$card) {
            throw new \Exception('Loyalty card is not found');
        }
        $card->client_id = $this->clientId;
        $card->card_number = $this->cardNumber ? $this->cardNumber : LoyaltyCard::find()->select('max(card_number)')->scalar() + 1;
        $card->card_type = $this->cardType;
        if (!$card->save()) {
            throw new \Exception('Error saving card. Number should be unique.');
        }
        return true;
    }
}
