<?php
namespace backend\controllers;

use backend\models\AddClientForm;
use backend\models\ClientSearch;
use common\models\Client;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class ClientController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'add', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionAdd()
    {
        $model = new AddClientForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->addClient()) {
            Yii::$app->session->setFlash('success', 'Client saved successfully');
            return $this->redirect(['index']);
        }

        return $this->render('add', [
            'model' => $model
        ]);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (!($client = Client::findOne($id)) || !$client->delete()) {
            throw new \Exception('Error deleting client');
        }
        Yii::$app->session->setFlash('success', 'Successfully deleted');
        return $this->redirect(['index']);
    }
}
