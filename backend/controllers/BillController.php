<?php
namespace backend\controllers;

use backend\models\AddBillForm;
use common\models\Bill;
use common\models\LoyaltyCard;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class BillController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'add', 'delete', 'report'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => Bill::find()])
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionAdd()
    {
        $model = new AddBillForm();

        if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $model->addBill()) {
            Yii::$app->session->setFlash('success', 'Bill saved successfully');
            return $this->redirect(['index']);
        }

        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (!($bill = Bill::findOne($id)) || !$bill->delete()) {
            throw new \Exception('Error deleting bill');
        }
        Yii::$app->session->setFlash('success', 'Successfully deleted');
        return $this->redirect(['index']);
    }

    /**
     * @return string
     */
    public function actionReport()
    {
        $thirtyDaysAsSeconds = 2592000;
        $nowTimestamp = time();
        $monthAgoTimestamp = $nowTimestamp - $thirtyDaysAsSeconds;



        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT sum(`total_sum`) as total_sum, `client_id` 
            FROM `bill` 
            LEFT JOIN `loyalty_card` ON 
            `bill`.`loyalty_card_id` = `loyalty_card`.`loyalty_card_id` 
            WHERE (`bill`.`created_at` BETWEEN :monthAgoTimestamp AND :nowTimestamp) 
            AND (NOT (`loyalty_card`.`client_id` IS NULL)) 
            GROUP BY `loyalty_card`.`client_id` 
            ORDER BY `total_sum` DESC LIMIT 10",
            [':monthAgoTimestamp' => $monthAgoTimestamp, ':nowTimestamp' => $nowTimestamp]
        );

        $result = $command->queryAll();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result
        ]);

        return $this->render('report', [
            'dataProvider' => $dataProvider
        ]);
    }
}
