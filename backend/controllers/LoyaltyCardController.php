<?php
namespace backend\controllers;

use backend\models\AddLoyaltyCardForm;
use backend\models\LoyaltyCardSearch;
use common\models\LoyaltyCard;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class LoyaltyCardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'edit', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LoyaltyCardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionEdit($id = null)
    {
        $model = new AddLoyaltyCardForm(['id' => $id]);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->addCard()) {
            Yii::$app->session->setFlash('success', 'Card saved successfully');
            return $this->redirect(['index']);
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (!($card = LoyaltyCard::findOne($id)) || !$card->delete()) {
            throw new \Exception('Error deleting loyalty card');
        }
        Yii::$app->session->setFlash('success', 'Successfully deleted');
        return $this->redirect(['index']);
    }
}
