<?php

/* @var $this \yii\web\View */

use common\models\Client;
use common\models\LoyaltyCard;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $dataProvider \yii\data\ActiveDataProvider */

?>

<div class="row">
    <div class="col-md-6">
        <h3>Total clients: <?= Client::find()->count() ?></h3>
    </div>
    <div class="col-md-6">
        <h3>Total loyalty cards: <?= LoyaltyCard::find()->count() ?></h3>
    </div>
</div>

<h2>Top 10 clients</h2>

<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'client_id',
        'total_sum',
    ]
]);
