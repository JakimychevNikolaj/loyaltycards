<?php

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\LoyaltyCardSearch */

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'bill_id',
        'total_sum',
        [
            'attribute' => 'loyalty_card_id',
            'label' => 'Loyalty Card',
            'value' => function ($model) {
                if (!$model->loyaltyCard) {
                    return '';
                }
                return Html::a($model->loyaltyCard->card_number, ['loyalty-card/index', 'LoyaltyCardSearch[card_number]' => $model->loyaltyCard->card_number]);
            },
            'format' => 'raw'
        ],
        'created_at:datetime',
        'updated_at:datetime',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template'=>'{delete}',
        ]
    ]
]);


