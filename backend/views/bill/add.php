<?php

use common\models\LoyaltyCard;
use common\models\Product;
use yii\db\Expression;
use yii\helpers\Html;

?>

<div>
    <h1>Add bill</h1>
    <div class="row" id="bill-form">
        <?= Html::beginForm(); ?>
        <div class="product-container">
            <div class="product-row">
                <div class="col-lg-5">
                    <label>Product</label>
                    <?= Html::dropDownList(
                        'productIds[]',
                        null,
                        Product::find()
                            ->select(new Expression('(CONCAT(name, "; price: ", price))'))
                            ->indexBy('product_id')
                            ->column(),
                        ['class' => 'form-control']
                    ) ?>
                </div>
                <div class="col-lg-5">
                    <label>Quantity</label>
                    <?= Html::textInput(
                        'productQuantities[]',
                        1,
                        ['type' => 'number', 'class' => 'form-control', 'min' => 0]
                    ) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <label>Loyalty card</label>
            <?= Html::dropDownList(
                'loyaltyCardId',
                null,
                LoyaltyCard::find()
                    ->select(new Expression('(CONCAT("Card number: ", card_number, " Client Id: ", client_id))'))
                    ->where(['NOT', ['client_id' => null]])
                    ->indexBy('loyalty_card_id')
                    ->column(),
                ['class' => 'form-control']
            ) ?>
            <div class="form-group" style="margin-top: 10px">
                <?= Html::button('Add product row', [
                    'class' => 'btn btn-success',
                    'onclick' => '$(".product-row").first().clone().appendTo(".product-container")'
                ]) ?>
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?= Html::endForm(); ?>
    </div>
</div>
</div>
