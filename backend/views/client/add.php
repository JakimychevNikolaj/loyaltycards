<?php

use common\models\LoyaltyCard;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<div>
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-lg-5">
            <h1>Add client</h1>
            <?= $form->field($model, 'firstName')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'lastName')->textInput() ?>

            <?= $form->field($model, 'address')->textInput() ?>

            <?= $form->field($model, 'email')->textInput() ?>

            <?= $form->field($model, 'phone')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="col-lg-5">
            <h1>Add loyalty card</h1>

            <?= $form->field($model, 'cardNumber')->textInput(['placeholder' => 'Select card number or it will be set automatically']) ?>

            <?= $form->field($model, 'cardType')->dropDownList(LoyaltyCard::getCardTypes(), ['prompt' => '-----']) ?>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
