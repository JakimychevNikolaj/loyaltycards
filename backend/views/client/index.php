<?php

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\ClientSearch */

use common\models\LoyaltyCard;
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'client_id',
        'first_name',
        'last_name',
        'address',
        'email',
        'phone',
        [
            'attribute' => 'card_number',
            'label' => 'Loyalty cards',
            'value' => function ($model) {
                $links = '';
                foreach ($model->loyaltyCards as $loyaltyCard) {
                    /** @var LoyaltyCard $loyaltyCard */
                    $links .= Html::a($loyaltyCard->card_number, ['loyalty-card/index', 'LoyaltyCardSearch[card_number]' => $loyaltyCard->card_number]) . '<br>';
                }
                return $links;
            },
            'format' => 'raw'
        ],
        'created_at:datetime',
        'updated_at:datetime',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template'=>'{delete}',
        ]
    ]
]);
