<?php

use common\models\LoyaltyCard;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<div>
    <h1>Add loyalty card</h1>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'cardNumber')->textInput(['autofocus' => true, 'placeholder' => 'Select card number or it will be set automatically']) ?>

            <?= $form->field($model, 'cardType')->dropDownList(LoyaltyCard::getCardTypes(), ['prompt' => '-----']) ?>

            <?= $form->field($model, 'clientId')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
