<?php

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\LoyaltyCardSearch */

use common\models\LoyaltyCard;
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'loyalty_card_id',
        'card_number',
        [
            'attribute' => 'card_type',
            'value' => function ($model) {
                return LoyaltyCard::getCardTypes()[$model->card_type];
            },
            'filter' => Html::activeDropDownList(
                $searchModel,
                'card_type',
                ['' => '-----'] + LoyaltyCard::getCardTypes(),
                ['class' => 'form-control']
            )
        ],
        [
            'attribute' => 'client_id',
            'value' => function ($model) {
                return Html::a($model->client_id, ['client/index', 'ClientSearch[client_id]' => $model->client_id]) . '<br>';
            },
            'format' => 'raw'
        ],
        'created_at:datetime',
        'updated_at:datetime',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template'=>'{edit} {delete}',
            'buttons' => [
                'edit' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['loyalty-card/edit', 'id' => $model->loyalty_card_id]);
                }
            ],
        ]
    ]
]);


