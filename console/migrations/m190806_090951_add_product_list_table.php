<?php

use common\models\Bill;
use common\models\Product;
use common\models\ProductList;
use yii\db\Migration;

/**
 * Class m190806_090951_add_product_list_table
 */
class m190806_090951_add_product_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = ProductList::tableName();
        $this->createTable($tableName, [
            'product_list_id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'bill_id' => $this->integer()->notNull(),
            'quantity' => $this->float()->defaultValue(1)->notNull(),
            'price' => $this->float()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            $tableName . '-' . Product::tableName() . '-' . 'fk',
            $tableName,
            'product_id',
            Product::tableName(),
            'product_id',
            'CASCADE'
        );
        $this->addForeignKey(
            $tableName . '-' . Bill::tableName() . '-' . 'fk',
            $tableName,
            'bill_id',
            Bill::tableName(),
            'bill_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = ProductList::tableName();
        $this->dropForeignKey($tableName . '-' . Product::tableName() . '-' . 'fk', $tableName);
        $this->dropForeignKey($tableName . '-' . Bill::tableName() . '-' . 'fk', $tableName);
        $this->dropTable($tableName);
    }
}
