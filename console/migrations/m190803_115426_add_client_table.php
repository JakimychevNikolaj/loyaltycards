<?php

use common\models\Client;
use yii\db\Migration;

/**
 * Class m190803_115426_add_client_table
 */
class m190803_115426_add_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Client::tableName(), [
            'client_id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'address' => $this->string(),
            'email' => $this->string()->notNull(),
            'phone' =>  $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Client::tableName());
    }
}
