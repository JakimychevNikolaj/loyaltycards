<?php

use common\models\Product;
use yii\db\Migration;

/**
 * Class m190806_083630_add_product_table
 */
class m190806_083630_add_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = Product::tableName();
        $this->createTable($tableName, [
            'product_id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->float()->notNull(),
            'barcode' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->insert($tableName, [
            'name' => 'potato',
            'price' => '30'
        ]);
        $this->insert($tableName, [
            'name' => 'tomato',
            'price' => '90'
        ]);
        $this->insert($tableName, [
            'name' => 'carrot',
            'price' => '45'
        ]);
        $this->insert($tableName, [
            'name' => 'apple',
            'price' => '50'
        ]);
        $this->insert($tableName, [
            'name' => 'orange',
            'price' => '60'
        ]);
        $this->insert($tableName, [
            'name' => 'bread',
            'price' => '25'
        ]);
        $this->insert($tableName, [
            'name' => 'butter',
            'price' => '75'
        ]);
        $this->insert($tableName, [
            'name' => 'water',
            'price' => '15'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Product::tableName());
    }
}
