<?php

use common\models\Bill;
use common\models\LoyaltyCard;
use yii\db\Migration;

/**
 * Class m190806_084922_add_bill_table
 */
class m190806_084922_add_bill_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = Bill::tableName();
        $this->createTable($tableName, [
            'bill_id' => $this->primaryKey(),
            'total_sum' => $this->float()->notNull(),
            'loyalty_card_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            $tableName . '-' . LoyaltyCard::tableName() . '-' . 'fk',
            $tableName,
            'loyalty_card_id',
            LoyaltyCard::tableName(),
            'loyalty_card_id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = Bill::tableName();
        $this->dropForeignKey($tableName . '-' . LoyaltyCard::tableName() . '-' . 'fk', $tableName);
        $this->dropTable($tableName);
    }
}
