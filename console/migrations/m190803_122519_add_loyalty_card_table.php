<?php

use common\models\Client;
use common\models\LoyaltyCard;
use yii\db\Migration;

/**
 * Class m190803_122519_add_loyalty_card_table
 */
class m190803_122519_add_loyalty_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = LoyaltyCard::tableName();

        $this->createTable($tableName, [
            'loyalty_card_id' => $this->primaryKey(),
            'card_number' => $this->integer()->unique()->notNull(),
            'card_type' => $this->tinyInteger()->notNull(),
            'client_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            $tableName . '-' . Client::tableName() . '-' . 'fk',
            $tableName,
            'client_id',
            Client::tableName(),
            'client_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = LoyaltyCard::tableName();
        $this->dropForeignKey($tableName . '-' . Client::tableName() . '-' . 'fk', $tableName);
        $this->dropTable($tableName);
    }
}
