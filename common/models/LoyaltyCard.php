<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * LoyaltyCard model
 *
 * @property integer $loyalty_card_id
 * @property string $card_number
 * @property integer $card_type
 * @property integer $client_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class LoyaltyCard extends ActiveRecord
{
    const TYPE_TEMPORARY = 0;
    const TYPE_PRIMARY = 1;

    const TYPE_TEMPORARY_LABEL = 'Temporary';
    const TYPE_PRIMARY_LABEL = 'Primary';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['card_number', 'card_type', 'client_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loyalty_card';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return array
     */
    public static function getCardTypes()
    {
        return [
            static::TYPE_TEMPORARY => static::TYPE_TEMPORARY_LABEL,
            static::TYPE_PRIMARY => static::TYPE_PRIMARY_LABEL,
        ];
    }
}
