<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * ProductList model
 *
 * @property integer $product_list_id
 * @property integer $product_id
 * @property integer $bill_id
 * @property float $quantity
 * @property float $price
 * @property integer $created_at
 * @property integer $updated_at
 */
class ProductList extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_list';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
