<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Bill model
 *
 * @property integer $bill_id
 * @property float $total_sum
 * @property integer $loyalty_card_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Bill extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bill';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyaltyCard()
    {
        return $this->hasOne(LoyaltyCard::className(), ['loyalty_card_id' => 'loyalty_card_id']);
    }
}
