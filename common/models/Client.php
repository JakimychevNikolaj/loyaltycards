<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Client model
 *
 * @property integer $client_id
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property integer $created_at
 * @property integer $updated_at
 */
class Client extends ActiveRecord
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone', 'email', 'address'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getLoyaltyCards()
    {
        return $this->hasMany(LoyaltyCard::className(), ['client_id' => 'client_id']);
    }
}